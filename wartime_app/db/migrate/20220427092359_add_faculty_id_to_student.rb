class AddFacultyIdToStudent < ActiveRecord::Migration[7.0]
  def change
    add_column :students, :faculty_id, :integer
    add_index :students, :faculty_id
  end
end
