class RemoveAvgRatingFromStudent < ActiveRecord::Migration[7.0]
  def up
    remove_column :students, :avg_rating
  end

  def down
    add_column :students, :avg_rating, :integer, default: 0, null: false
  end
end
