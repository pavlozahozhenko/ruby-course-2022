class AddAvgRatingToStudent < ActiveRecord::Migration[7.0]
  def up
    add_column :students, :avg_rating, :integer, default: 0, null: false
    add_index :students, :avg_rating
    ::Student.find_each do |student|
      say_with_time("Updating student #{student.id}") do
        student.update_column(:avg_rating, 61)
      end
    end
  end

  def down
    remove_column :students, :avg_rating
  end
end
