class CreateStudents < ActiveRecord::Migration[7.0]
  def change
    create_table :students do |t|
      t.string :first_name, length: 100, null: false, default: ''
      t.string :last_name, length: 100, null: false, default: ''
      t.string :email, length: 200
      t.timestamps
    end
  end
end
