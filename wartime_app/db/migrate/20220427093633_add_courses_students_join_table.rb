class AddCoursesStudentsJoinTable < ActiveRecord::Migration[7.0]
  def change
    create_table :courses_students, id: false do |t|
      t.references :course
      t.references :student
    end
  end
end
