class CreateGradeBooks < ActiveRecord::Migration[7.0]
  def change
    create_table :grade_books do |t|
      t.float :avg_rating, null: false, default: 61
      t.references :student
      t.timestamps
    end
  end
end
