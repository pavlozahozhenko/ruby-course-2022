FactoryBot.define do
  factory :student do
    first_name { ::FFaker::Name.first_name }
    last_name  { 'Man' }
    email { ::FFaker::Internet.email }
    password { 'password' }
    password_confirmation { 'password' }
    faculty
  end
end
