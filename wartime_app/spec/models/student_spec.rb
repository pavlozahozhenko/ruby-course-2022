require 'rails_helper'

describe Student, type: :model do
  subject { ::FactoryBot.build(:student) }
  
  it 'should have a valid factory' do
    expect(subject).to be_valid
  end

  describe '#full_name' do
    subject { ::FactoryBot.create(:student, first_name: 'Test') }

    it 'should return first name + last name' do
      expect(subject.full_name).to eq('Test Man')
    end

    it 'should return first name only if last name is not present' do
      subject.update_column(:last_name, '')
      expect(subject.full_name).to eq('Test')
    end

    it 'should return last name only if first name is not present' do
      subject.update_column(:first_name, '')
      expect(subject.full_name).to eq('Man')
    end
  end
end
