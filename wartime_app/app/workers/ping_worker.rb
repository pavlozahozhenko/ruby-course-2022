class PingWorker
  include Sidekiq::Job

  def perform(student_id, seconds)
    student = ::Student.find student_id
    logger.info "Pinging student #{student.full_name}..."
    sleep seconds
  end
end
