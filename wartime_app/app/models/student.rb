class Student < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :first_name, :last_name, :email, presence: true
  validates :first_name, length: { in: 2..100 }

  has_one :grade_book
  belongs_to :faculty
  has_and_belongs_to_many :courses

  before_save :do_something
  after_create do |record|
    puts "===created student #{record.full_name}==="
  end

  def full_name
    "#{first_name} #{last_name}".strip
  end

  def do_something
    puts 'Doing something...'
  end

  def avg_rating
    grade_book&.avg_rating
  end

  def is_admin?
    role == 'admin'
  end
end
