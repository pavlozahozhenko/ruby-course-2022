class HomeController < ApplicationController
  before_action :authenticate_student!, except: [:index]
  
  def index
    @students = ::Student.accessible_by(Ability.new(current_student))
  end

  def students
    student = ::Student.find params[:id]
    @name = student.full_name
  end
end
