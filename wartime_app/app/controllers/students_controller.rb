class StudentsController < ApplicationController
  def ping
    @student = ::Student.find params[:id]
    jid = ::PingWorker.perform_async(@student.id, 8)
    render plain: 'pinged...'
  end
end
