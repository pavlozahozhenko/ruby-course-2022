Given /^student exists$/ do
  @student = ::FactoryBot.create :student, email: 'test@example.com'
end

Given /^admin exists$/ do
  @student = ::FactoryBot.create :student, email: 'test@example.com', role: 'admin'
end

Given /^student is logged in$/ do
  login_as(@student)
end

Given /^student is an? admin$/ do
  @student.update_column(:role, :admin)
end

Given /the student exists with email "(.*)"/ do |email|
  ::FactoryBot.create(:student, email: email)
end

Then /there should exist a student with the name "(.*)"/ do |first_name|
  student = ::Student.find_by(first_name: first_name)
  expect(student).not_to be_nil
end
