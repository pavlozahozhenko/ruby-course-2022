Feature: Main flow

  Background:
    Given student exists
    And I am on login page
    When I fill in "student[email]" field with "test@example.com"
    And I fill in "student[password]" field with "password"
    And I click on "Sign in"

  @javascript
  Scenario: Student logs into the system
    Then I should see text "Students list"

  @javascript
  Scenario: Admin logs into the system and pings an existing student
    Given student is an admin
    And the student exists with email "to_edit@example.com"
    When I go to root
    And I click on the last "Ping" within ".actions"
    Then I should see text "pinged..."
