Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  devise_for :students
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"

  root to: 'home#index'

  get '/students/:id', to: 'home#students'

  resources :students do
    member do
      get :ping
    end
  end

  resources :todos, only: %i[index create destroy] do
    member do
      get :mark_as_done
    end
  end
end
