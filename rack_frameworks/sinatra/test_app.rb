require 'sinatra'

get '/hello/username' do
  "Hello, world!"
end

get '/hello/:name' do |name|
  request
  erb :hello, locals: {name: name}
end

post '/hello' do
  'hello post'
end

options '/hello' do
  'hello from options'
end
