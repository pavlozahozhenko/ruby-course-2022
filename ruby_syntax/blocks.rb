class A
  def method_with_block
    @c = 'local value'
    puts "self: #{self}"
    puts 'smth is ongoing...'
    a = 2 * 3
    if block_given?
      puts 'block detected...'
      b = yield(a)
    end
    b * 3
  end

  def multiproc(arr)
    arr.map { |p| p.call }
  end

  def method_missing(name, *args, &block)
    puts 'method missing called!'
    if name =~ /foo\d+/
      puts "#{name} method implementation goes here..."
    else
      super
    end
  end

  [:foo, :baz, :bar].each do |method_name|
    define_method(method_name) do
      puts "#{method_name} implementation goes here..."
    end
  end
end
