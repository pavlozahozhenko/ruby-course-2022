module Biology
  class Animal
    def move(distance)
      puts "trying to move... #{distance}"
      return 0 if distance < 0
      distance
    end
  end

  class Rodent < Animal
    TEETH_GAP = 0.2

    def eat(food)
      puts 'eating... chew chew chew...'
      increase_weight(1)
    end

    def initialize
      @@rodents_num ||= 0
      @@rodents_num += 1
    end

    def total_rodents
      "Rodents population: #{@@rodents_num}"
    end

    private

    def increase_weight(n)
      @weight ||= 1
      @weight += n
    end
  end

  module DiggingAbility
    attr_accessor :depth

    def dig(depth)
      puts 'digging....'
      @depth ||= 0
      @depth -= depth
    end

    def self.about
      puts 'This module adds some digging ability'
    end
  end

  class Groundhog < Rodent
    include DiggingAbility
    TEETH_GAP = 0.4

    attr_reader :age, :weight
    attr_writer :weight
    attr_accessor :name

    def initialize(name = nil, age = 0, weight = 0)
      super()
      @name = name
      @age = age
      @weight = weight
      @depth = -3
      puts "init groundhog... #{name}"
    end

    def move(distance, speed: 1)
      v = super(distance)
      "#{@name} is moving..."
    end

    def eat(food)
      super
      puts "Groundhog teeth gap: #{TEETH_GAP}"
    end

    def self.description
      'A nice rodent'
    end
  end

  class PrairieDog < Rodent
    include DiggingAbility

    def eat(food)
      super
      puts "Teeth gap: #{TEETH_GAP}"
    end

    def bite(prairie_dog)
      puts 'biting!'
      prairie_dog.jump(10)
    end

    protected

    def jump(height)
      puts "jumping #{height}..."
    end
  end

  class Capybara < Rodent
  end
end

module Onsen
  class Capybara
  end
end
