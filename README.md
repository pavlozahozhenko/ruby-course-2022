# Ruby Course 2022

NaUKMA Ruby on Rails programming course 2022

### Announcements

### Course Materials
* [Telegram channel](http://t.me/ruby_2022)
* [Repository 2021 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2021)
* [Repository 2020 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2020)
* [Repository 2019 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2019)
* [Repository 2017 (BitBucket)](https://bitbucket.org/burius/ruby_course_2017)
* [Repository 2016 (BitBucket)](https://bitbucket.org/burius/ror_course)

### The Project
* [Project requirements (TBD, linking 2021 requirements)](https://gitlab.com/pavlozahozhenko/ruby-course-2021/-/tree/master/students#project-requirements-info)

### Useful Links
#### Ruby/Rails
* [Official Ruby documentation (core API)](http://ruby-doc.org/core-3.0.0/)
* [Ruby style guide](https://rubystyle.guide/) by bbatsov
* [Rails API documentation](http://api.rubyonrails.org/)
* [Official Rails Guides](https://guides.rubyonrails.org/)
